module IrQL.Parser where

import Control.Applicative hiding (many, (<|>))
import Text.Parsec hiding (Line)
import Text.Parsec.Language
import qualified Text.Parsec.Token as P

type Parser = Parsec String ()


type Lang = [Line]
data Line = Filter Expr
          | GroupBy Expr
          | Map     Expr
          | SortBy  Expr Bool
          | Fork    Expr
          | Return  Expr
          | Sum
          | DropWhile Expr
          | TakeWhile Expr
          | To      Variable
          deriving (Show, Eq)

type Variable = String

data Expr = OneExpr OneLine
          | Block Lang
          deriving (Show, Eq)

data OneLine = JsonDict [(OneLine, OneLine)]
             | JsonList [OneLine]
             | BinOp BinOps OneLine OneLine
             | UnOp UnOps OneLine
             | Val Value
             deriving (Show, Eq)

data Value = Int_ Integer
           | Float_ Double
           | String_ String
           | Boolean_ Bool
           | Param Id [Rest]
           deriving (Show, Eq)

data Id = Var Variable
        | This
        | That
        deriving (Show, Eq)

data Rest = Dot String
          | Idx OneLine
          deriving (Show, Eq)

data BinOps = Or | Plus | Mult | Eq | NEq | Gt | GtE | Lt | LtE deriving (Show, Eq)
data UnOps  = Not deriving (Show, Eq)

langDef = emptyDef
            { P.commentStart     = "/*"
            , P.commentEnd       = "*/"
            , P.commentLine      = "//"
            , P.nestedComments   = False
            , P.identStart       = letter <|> char '_'
            , P.reservedNames    = [ "filter", "group", "by", "sort"
                                   , "map", "fork", "to", "return"
                                   , "take", "drop", "while", "sum"
                                   , "and", "or", "not"
                                   , "false", "true"]
            }

lexer = P.makeTokenParser langDef

parens      = P.parens lexer
braces      = P.braces lexer
brackets    = P.brackets lexer
identifier  = P.identifier lexer
reserved    = P.reserved lexer
symbol      = P.symbol lexer
commaSep    = P.commaSep lexer

language = do
    a <- lang
    eof
    return a

lang :: Parser Lang
lang = many line
line = choice
    [ filter_
    , map_
    , sortby
    , return_
    , groupby
    , takewhile
    , dropwhile
    , to_
    , reserved "sum" >> return Sum
    ]

filter_ = reserved "filter" >> Filter <$> expr
map_    = reserved "map"    >> Map <$> expr
return_ = reserved "return" >> Return <$> expr
groupby = reserved "group" >> reserved "by" >> GroupBy <$> expr
takewhile = reserved "take" >> reserved "while" >> TakeWhile <$> expr
dropwhile = reserved "drop" >> reserved "while" >> DropWhile <$> expr
to_     = reserved "to"     >> To <$> identifier
sortby  = do
    reserved "sort" >> reserved "by" 
    e <- expr
    asc <- choice
        [ try (reserved "asc") >> return True
        , reserved "desc" >> return False
        ]
    return (SortBy e asc)


expr = block <|> (OneExpr <$> oneline)

block = do
    try $ symbol "\\{"
    lng <- lang
    symbol "}"
    return $ Block lng

oneline = choice
    [ JsonDict <$> braces jsonDict
    , JsonList <$> brackets jsonList
    , math
    ]

jsonDict = commaSep $ do
    key <- math
    symbol ":"
    value <- oneline
    return (key, value)

jsonList = commaSep oneline 

math = p0
p0 = binopl (reserved "or") (BinOp Or) p2

p2 = binopl (symbol "==") (BinOp Eq) p3
p3 = binopr (symbol "*") (BinOp Mult) p4
p4 = binopr (symbol "+") (BinOp Plus) p5

p5 = choice
    [ parens oneline
    , (try $ reserved "not") >> UnOp Not <$> p5
    , Val <$> value
    ]

binopl, binopr :: Parser a
      -> (OneLine ->  OneLine -> OneLine)
      -> Parser OneLine
      -> Parser OneLine
binopl smb cons p = choice
    [ try $ do
            a <- p
            smb
            b <- binopl smb cons p
            return (cons a b)
    , p
    ]

binopr smb cons p = p >>= binopr'
  where
    binopr' acc = choice
        [ do
            try smb
            b <- p
            let acc' = cons acc b
            binopr' acc'
        , return acc
        ]
                
value :: Parsec String () Value
value = choice
    [ try (Float_ <$> P.float lexer)
    , Int_   <$> P.integer lexer
    , String_ <$> P.stringLiteral lexer
    , try (reserved "true") >> (return $ Boolean_ True)
    , try (reserved "false") >> (return $ Boolean_ False)
    , param
    ]

param = do
    id <- try $ choice
            [ symbol "$" >> return This
            , symbol "@" >> return That
            , Var <$> identifier
            ]
    rest <- many $ choice
            [ Idx <$> brackets oneline
            , try (symbol ".") >> Dot <$> identifier
            ]
    return (Param id rest)

