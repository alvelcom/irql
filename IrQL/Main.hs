{-# LANGUAGE OverloadedStrings #-}

import qualified IrQL.Parser as P
import IrQL.Parser hiding (Value)

import Text.Parsec as PP (parse)
import Data.Text
import Data.Map as M
import Data.HashMap.Strict as H
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Data.Scientific
import Data.Aeson as A
import Data.List as L

import qualified Data.ByteString.Lazy as BS

import qualified Data.Vector as V

import System.Environment
import System.IO

import Debug.Trace

type Env  = M.Map Text Value
type EnvS = State Env


evalLang :: Lang -> Value -> EnvS Value
evalLang lang value = foldM (flip evalLine) value lang

evalLine :: Line -> Value -> EnvS Value
evalLine line value =
    case line of
        To var      -> evalTo var value
        Filter expr -> evalFilter expr value
        Map    expr -> evalMap expr value
        Return expr -> evalReturn expr value
        GroupBy expr -> evalGroupBy expr value
        SortBy expr order -> evalSortBy expr value order
        TakeWhile expr -> evalTakeWhile expr value
        DropWhile expr -> evalDropWhile expr value
        Sum -> evalSum value

evalExpr :: Expr -> Value -> EnvS Value
evalExpr expr value =
    case expr of
        OneExpr ol -> evalOneLine ol value
        Block lang -> evalLang lang value

evalOneLine :: OneLine -> Value -> EnvS Value
evalOneLine op value =
    case op of
        JsonDict d -> evalJsonDict d value
        JsonList l -> evalJsonList l value
        BinOp bop a b -> calcBinOp bop <$> evalOneLine a value <*> evalOneLine b value
        UnOp uop a -> calcUnOp uop <$> evalOneLine a value
        Val val -> evalVal val value
        
evalJsonDict :: [(OneLine, OneLine)] -> Value -> EnvS Value
evalJsonDict dict value = do 
    let f (key, val) = do
            String key' <- evalOneLine key value
            val' <- evalOneLine val value
            return (key', val')
    list <- mapM f dict
    return $ Object $ H.fromList list

evalJsonList :: [OneLine] -> Value -> EnvS Value
evalJsonList lst value =
    mapM (flip evalOneLine value) lst >>= return . Array . V.fromList
    
calcUnOp | trace "calcUnOp" False = undefined

calcBinOp :: P.BinOps -> Value -> Value -> Value
calcBinOp Plus (Number a) (Number b) = Number (a + b)
calcBinOp Mult (Number a) (Number b) = Number (a * b)
calcBinOp Or   (Bool a)   (Bool b)   = Bool (a || b)
calcBinOp Eq   a          b          = Bool (a == b)


evalVal :: P.Value -> Value -> EnvS Value
evalVal val value =
    case val of
        Int_ int -> return $ Number $ fromInteger $ toInteger int
        Float_ float -> return $ Number $ fromRational $ toRational float
        String_ str -> return $ A.String $ pack str
        Boolean_ bool -> return $ Bool bool
        Param var rest ->  evalRest rest value =<< evalId var value
            

evalRest :: [Rest] -> Value -> Value -> EnvS Value
evalRest rests value a = foldM f a rests
  where
    f (Array v) (Idx ol) = do
        index <- evalOneLine ol value
        case index of
            (Number n) -> let int = sciToInt n
                              idx = if int >= 0 then int else V.length v + int
                          in return (v V.! idx)
    f (Object o) (Dot name) = do
        case H.lookup (pack name) o of
            Just obj -> return obj
            q | trace (show (q, name)) False -> undefined
    f (Array v) (Dot "length") =
        return $ Number $ fromInteger $ toInteger $ V.length v
    f (Object o) (Idx ol) = do
        index <- evalOneLine ol value
        case index of
            (String str) -> f (Object o) (Dot $ unpack str)

sciToInt :: Scientific -> Int
sciToInt s = fromIntegral (coefficient s * 10 ^ base10Exponent s)

evalId :: Id -> Value -> EnvS Value
evalId This a = return a
evalId (Var name) _ = do
    state <- get
    case M.lookup (pack name) state of
        Just val -> return val

pureEvalExpr :: Env -> Expr -> Value -> Value
pureEvalExpr env expr value = fst $ runState (evalExpr expr value) env

-----
--
-----

evalTo :: Variable -> Value -> EnvS Value
evalTo var value = do
    modify (M.insert (pack var) value)
    return value
 
evalFilter :: Expr -> Value -> EnvS Value
evalFilter expr (Array arr) = do
    state <- get
    let pred value = case pureEvalExpr state expr value of
                        Bool bool -> bool
    return $ Array $ V.filter pred arr

evalReturn expr value = do
    state <- get
    return $ pureEvalExpr state expr value

evalMap expr (Array arr) = do
    state <- get
    return $ Array $ V.map (pureEvalExpr state expr) arr

evalTakeWhile expr (Array arr) = do
    state <- get
    let pred value = case pureEvalExpr state expr value of
                        Bool bool -> bool
    return $ Array $ V.takeWhile pred arr

evalDropWhile expr (Array arr) = do
    state <- get
    let pred value = case pureEvalExpr state expr value of
                        Bool bool -> bool
    return $ Array $ V.dropWhile pred arr

evalSum (Array arr) = do
    return $ Number $ V.sum $ V.map deNumber arr
  where
    deNumber (Number a) = a 

evalGroupBy expr (Array arr) = do
    state <- get
    let grouper a b = (pureEvalExpr state expr a) == (pureEvalExpr state expr b)
    let grouped = L.groupBy grouper $ V.toList arr
    return $ Array $ V.fromList $ L.map (Array . V.fromList) grouped


evalSortBy :: Expr -> Value -> Bool -> EnvS Value
evalSortBy expr (Array arr) order = do
    state <- get
    let extract = pureEvalExpr state expr
    let cmp a b = compare (extract a) (extract b)
    let list = V.fromList $ L.sortBy cmp $ V.toList arr
    return $ Array $ if order then list else V.reverse list

instance Ord Value where
    compare (Array a) (Array b) = compare a b
    compare (Object a) (Object b) = EQ
    compare (Number a) (Number b) = compare a b
    compare (Bool a) (Bool b) = compare a b
    compare (String a) (String b) = compare a b


parse str = PP.parse P.language "" str


main = do
    args <- getArgs
    (name, sources) <- case args of
            [sourceFile] -> do
                txt <- Prelude.readFile sourceFile
                return (sourceFile, txt)
            ("-e" : txt) -> return ("<eval>", L.unwords txt)
    case PP.parse P.language name sources of 
        Right lang -> performe lang
        Left msg -> print msg


performe lang = do
    input <- BS.hGetContents stdin
    case decode input of
        Just value -> do
            BS.hPutStr stdout $ encode $ fst $ runState (evalLang lang value) M.empty
        Nothing -> putStrLn "Bad json"
 
